FROM python:3.7

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install Python and Package Libraries
RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get autoremove \
 && apt-get autoclean \
 && apt-get install libpq-dev -y

# Project Directory and Settings
ARG PROJECT=nyc-metro-tracker
ARG PROJECT_DIR=/code/

# Set the working directory to /code
RUN mkdir -p $PROJECT_DIR
COPY . $PROJECT_DIR
WORKDIR $PROJECT_DIR

# Install necessary python packages
RUN pip3 install -r requirements.txt
