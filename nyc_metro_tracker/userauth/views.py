from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import CreateView

from service_status.models import Service
from userauth.models import Profile


class CreateUser(CreateView):
    model = User
    fields = ['first_name', 'last_name', 'username', 'email', 'password']
    template_name = 'auth/create.html'

    def check_logged_in(self):
        return self.request.user.is_authenticated()

    def get_success_url(self):
        return reverse('create-profile', user_id=self.object.pk)

    def get(self, request, *args, **kwargs):
        if self.check_logged_in():
            raise PermissionDenied()
        return super().get(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if self.check_logged_in():
            raise PermissionDenied()
        return super().post(self, request, *args, **kwargs)


def create_profile(request, user_id):
    profile = Profile.objects.get_or_create(user=get_object_or_404(User, pk=user_id))
    profile[0].save()
    return redirect('/')


@login_required(login_url='login')
def favorite_service(request, service_id):
    profile = request.user.profile
    service = get_object_or_404(Service, pk=service_id)
    profile.favorites.add(service)
    return redirect('/')


@login_required(login_url='login')
def unfavorite_service(request, service_id):
    profile = request.user.profile
    service = get_object_or_404(Service, pk=service_id)
    profile.favorites.remove(service)
    return redirect('/')
