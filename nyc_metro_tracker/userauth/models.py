from django.contrib.auth.models import User
from django.db import models

from service_status.models import Service


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    favorites = models.ManyToManyField(Service)
