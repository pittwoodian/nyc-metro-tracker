from django.urls import path

from userauth import views

urlpatterns = [
    path('create/', views.CreateUser.as_view(), name='create-user'),
    path('create-profile/<int:user_id>/', views.create_profile, name='create-profile'),
    path('add-favorite/<int:service_id>/', views.favorite_service, name='favorite-service'),
    path('remove-favorite/<int:service_id>/', views.unfavorite_service, name='unfavorite-service')
]