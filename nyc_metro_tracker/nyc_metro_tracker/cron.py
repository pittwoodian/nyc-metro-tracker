import datetime
import re
import urllib

import pytz
import xmltodict as xmltodict
from django_cron import CronJobBase, Schedule

from service_status.models import Service

SERVICE_URL = 'http://web.mta.info/status/serviceStatus.txt'


class MTAServiceUpdate(CronJobBase):
    RUN_EVERY_MINS = 10
    schedule = Schedule(run_every_mins=10)
    code = 'nyc_metro_tracker.MTAServiceUpdate'

    def do(self):
        with urllib.request.urlopen(SERVICE_URL) as page:
            page_data = page.read().decode('utf8')

        mta_data_dict = xmltodict.parse(page_data)
        for service_type, service_data in mta_data_dict['service'].items():
            if service_type == 'responsecode' or service_type == 'timestamp':
                continue
            for line in service_data['line']:
                obj, found = Service.objects.get_or_create(service_type=service_type, name=line['name'])

                obj.status = line['status'] or ''

                text = line['text']
                if text:
                    text = text.replace('<br/>', '\n')
                    text = text.replace('&nbsp;', ' ')
                    text = re.sub("<.*?>", "", text)
                obj.text = text or ''

                if line['Date'] and line['Time']:
                    combined_date_time = ' '.join([line['Date'], line['Time']])
                    date_time_obj = datetime.datetime.strptime(combined_date_time, '%m/%d/%Y %I:%M%p')
                    obj.datetime = date_time_obj

                obj.save()
