from django.urls import path

from service_status import views

urlpatterns = [
    path('', views.ServiceList.as_view(), name='index'),
    path('details/<int:pk>', views.ServiceDetails.as_view(), name='details'),
    path('favorites/', views.FavoriteServiceList.as_view(), name='favorites')
]
