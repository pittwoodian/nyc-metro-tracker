from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import ListView, DetailView

from service_status.models import Service


class ServiceList(ListView):
    model = Service
    template_name = 'service_status/list.html'

    def get_queryset(self):
        filter_val = self.request.GET.get('filter', '')
        if filter_val:
            qs = Service.objects.filter(service_type=filter_val)
        else:
            qs = Service.objects.all()
        return qs


class ServiceDetails(DetailView):
    model = Service
    template_name = 'service_status/details.html'


class FavoriteServiceList(LoginRequiredMixin, ListView):
    model = Service
    template_name = 'service_status/list.html'

    def get_queryset(self):
        user = self.request.user
        queryset = user.profile.favorites.all()
        return queryset
