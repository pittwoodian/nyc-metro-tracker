from django.db import models


SERVICE_TYPES = (
    ('subway', 'Subway'),
    ('bus', 'Bus'),
    ('BT', 'Bridge/Tunnel'),
    ('LIRR', 'Long Island Railroad'),
    ('MetroNorth', 'Metro-North Railroad')
)


class Service(models.Model):
    service_type = models.CharField(choices=SERVICE_TYPES, max_length=32)
    name = models.CharField(unique=True, max_length=32)
    status = models.CharField(max_length=32, blank=True)
    text = models.TextField(blank=True)
    datetime = models.DateTimeField(blank=True, null=True)
