from django.contrib import admin

from service_status.models import Service


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('service_type', 'name', 'status')
    readonly_fields = ['id']


admin.site.register(Service, ServiceAdmin)
